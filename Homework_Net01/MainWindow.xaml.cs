﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Homework_Net01
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void IpButtonClick(object sender, RoutedEventArgs e)
        {
            if (ipTextBox.Text == "")
            {
                MessageBox.Show("Введите IP адрес!");
            }
            else
            {
                try
                {
                    string ipAddress = ipTextBox.Text;
                    IPHostEntry hostName = Dns.GetHostEntry(ipAddress);
                    ipNameTextBox.Text = hostName.HostName;
                }
                catch(SocketException ex)
                {
                    MessageBox.Show("Неверный адрес!");
                }
            }
        }

        private void NameButtonClick(object sender, RoutedEventArgs e)
        {
            if (nameTextBox.Text == "")
            {
                MessageBox.Show("Введите имя сервера!");
            }
            else
            {
                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    string hostName = nameTextBox.Text;
                    try
                    {
                        IPHostEntry ipAddress = Dns.GetHostEntry(hostName);
                        string content = "";
                        for (int i = 0; i < ipAddress.AddressList.Length; i++)
                        {
                            
                            content += ipAddress.AddressList[i].ToString() + "\n";
                            //nameIpTextBox.Text = ipAddress.AddressList[i].ToString();
                        }
                        //nameIpTextBox.Text = ipAddress.AddressList[0].ToString();
                        nameIpTextBox.Text = content;
                    }
                    catch (SocketException ex)
                    {
                        MessageBox.Show("Неверно написано имя!");
                    }
                }
            } 
        }
    }
}
